const formH2 = document.querySelector("form h2")
const formPara = document.querySelector("form p")
const button1 = document.querySelector(".button1")
const button2 = document.querySelector(".button2")
const button3 = document.querySelector(".button3")
const button4 = document.querySelector(".button4")
const choosePlan = document.querySelector(".choose-plan")
const personalInfo = document.querySelector(".personal-info")
const goBackButton = document.querySelector(".go-back-button")
const nextStepContainer = document.querySelector(".next-step-container")
const optionPrice1 = document.querySelector(".option-price-1")
const optionPrice2 = document.querySelector(".option-price-2")
const optionPrice3 = document.querySelector(".option-price-3")
const finishingUpForm = document.querySelector('.finishing-up-form')
const thankYouPage = document.querySelector('.thank-you-page')
const nextStepButton = document.querySelector(".next-step-button")
const Name = document.querySelector("#name")
const email = document.querySelector("#email")
const phoneNumber = document.querySelector("#phone")
const twoMonthsFree = document.querySelectorAll(".two-months-free")
const optionPriceYear = document.querySelectorAll(".option-price-year")
const optionPriceMonthly = document.querySelectorAll(".option-price-monthly")
const toggleButton = document.querySelector(".toggle")
const planOption1 = document.querySelector('.plan-option-1')
const planOption2 = document.querySelector('.plan-option-2')
const planOption3 = document.querySelector('.plan-option-3')
const pickAddOns = document.querySelector('.pick-add-ons')
const pickAddOnsPriceYearly = document.querySelectorAll('.pick-add-ons-price-yearly')
const pickAddOnsPriceMonthly = document.querySelectorAll('.pick-add-ons-price-monthly')
const toggleButton2 = document.querySelector(".toggle2")
const pickAddOnsInput1 = document.querySelector("#pick-add-ons-input-1")
const pickAddOnsInput2 = document.querySelector("#pick-add-ons-input-2")
const pickAddOnsInput3 = document.querySelector("#pick-add-ons-input-3")
let onlineService = false
let largerStorage = false
let customizableProfile = false
let selectedValue = 0
let isMonthly = true
let isMonthlyInPickAddOns = true


const step1 = () => {

    // Managing the layout of the first page
    button1.classList.toggle("button-glow")
    choosePlan.classList.toggle("hide-display")
    pickAddOns.classList.toggle("hide-display")
    // personalInfo.classList.toggle("hide-display")
    finishingUpForm.classList.toggle("hide-display")
    thankYouPage.classList.toggle("hide-display")
    goBackButton.classList.toggle("hide-display")

    // Calling the step 2 on the basis of user behaviour
    nextStepButton.addEventListener('click', nextStepFunction1)
}

const nextStepFunction1 = () => {

    const nameValue = Name.value;
    const emailValue = email.value;
    const phoneNumberValue = phoneNumber.value;

    if (nameValue != "" && emailValue != "" && phoneNumberValue != "") {
        step2()
    }
    else {
        if (nameValue == "") {
            const errorSpan1 = document.querySelector(".error-span-1")
            errorSpan1.style.display = 'block'
            Name.classList.toggle('border-red')
        }
        if (emailValue == "") {
            const errorSpan2 = document.querySelector(".error-span-2")
            errorSpan2.style.display = 'block'
            email.classList.toggle('border-red')
        }
        if (phoneNumberValue == "") {
            const errorSpan3 = document.querySelector(".error-span-3")
            errorSpan3.style.display = 'block'
            phoneNumber.classList.toggle('border-red')
        }
    }
}

const step2 = () => {

    // Step 2 Monthly
    // Switching the glow of the button
    button1.classList.toggle("button-glow")
    button2.classList.toggle("button-glow")

    personalInfo.classList.toggle("hide-display")
    choosePlan.classList.toggle("hide-display")

    // Displaying the Go Back Button in the footer
    goBackButton.classList.toggle("hide-display")
    nextStepContainer.style.justifyContent = "space-between"

    optionPriceYear.forEach((eachElement) => {
        eachElement.classList.toggle("hide-display")
    })

    twoMonthsFree.forEach((eachElement) => {
        eachElement.classList.toggle("hide-display")
    })

    //Getting the input -> Arcade, Advanced or Pro
    const radioButtons = document.querySelectorAll('.plan-input');

    radioButtons.forEach(button => {
        button.addEventListener('change', function () {

            if (this.checked) {
                // Get the value of the selected radio button
                selectedValue = this.value;
                console.log("Selected value:", selectedValue);
            }
        });
    });

    toggleButton.addEventListener('click', toggleMonthYearInStep2)

    // Removing previous event listener
    nextStepButton.removeEventListener('click', nextStepFunction1)
    nextStepButton.addEventListener('click', step3)

    goBackButton.addEventListener('click', goBackTostep1)
}

const goBackTostep1 = () => {

    button1.classList.toggle("button-glow")
    button2.classList.toggle("button-glow")

    personalInfo.classList.toggle("hide-display")
    choosePlan.classList.toggle("hide-display")

    goBackButton.classList.toggle("hide-display")
    nextStepContainer.style.justifyContent = "flex-end"

    optionPriceYear.forEach((eachElement) => {
        eachElement.classList.toggle("hide-display")
    })

    twoMonthsFree.forEach((eachElement) => {
        eachElement.classList.toggle("hide-display")
    })

    nextStepButton.removeEventListener('click', step3)
    nextStepButton.addEventListener('click', nextStepFunction1)
}

const toggleMonthYearInStep2 = (e) => {

    const checkBtn = document.querySelector(`#${e.target.htmlFor}`);
    if (!checkBtn.checked) {
        // If Yearly

        isMonthly = false
        optionPriceMonthly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        optionPriceYear.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        twoMonthsFree.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        toggleButton.classList.toggle("shift-end");
    } else {
        // If Monthly

        isMonthly = true
        optionPriceMonthly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        twoMonthsFree.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        optionPriceYear.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display");
        });
        toggleButton.classList.toggle("shift-end");
    }
};

const step3 = () => {

    // Step 3 Monthly

    // Switching the glow of the button
    button2.classList.toggle("button-glow")
    button3.classList.toggle("button-glow")

    choosePlan.classList.toggle('hide-display')
    pickAddOns.classList.toggle('hide-display')

    pickAddOnsPriceYearly.forEach((eachElement) => {

        eachElement.classList.toggle('hide-display')
    })

    pickAddOnsInput1.addEventListener('change', function () {

        if (this.checked) {
            onlineService = true
        }
        else {
            onlineService = false
        }
    })

    pickAddOnsInput2.addEventListener('change', function () {

        if (this.checked) {
            largerStorage = true
        }
        else {
            largerStorage = false
        }
    })

    pickAddOnsInput3.addEventListener('change', function () {

        if (this.checked) {
            customizableProfile = true
        }
        else {
            customizableProfile = false
        }
    })

    toggleButton2.addEventListener('click', toggleMonthYearInStep3)

    nextStepButton.removeEventListener('click', step3)
    nextStepButton.addEventListener('click', step4)

    goBackButton.removeEventListener('click', goBackTostep1)
    goBackButton.addEventListener('click', goBackTostep2)
}

const goBackTostep2 = () => {

    button2.classList.toggle("button-glow")
    button3.classList.toggle("button-glow")

    choosePlan.classList.toggle('hide-display')
    pickAddOns.classList.toggle('hide-display')

    pickAddOnsPriceYearly.forEach((eachElement) => {

        eachElement.classList.toggle('hide-display')
    })

    nextStepButton.removeEventListener('click', step4)
    nextStepButton.addEventListener('click', step3)

    goBackButton.removeEventListener('click', goBackTostep2)
    goBackButton.addEventListener('click', goBackTostep1)
}

const toggleMonthYearInStep3 = (e) => {

    const checkBtn = document.querySelector(`#${e.target.htmlFor}`)
    if (!checkBtn.checked) {
        // If yearly

        isMonthlyInPickAddOns = false
        toggleButton2.classList.toggle("shift-end");

        pickAddOnsPriceYearly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display")
        })

        pickAddOnsPriceMonthly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display")
        })
    }
    else {

        // If Monthly
        isMonthlyInPickAddOns = true
        toggleButton2.classList.toggle("shift-end");

        pickAddOnsPriceYearly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display")
        })

        pickAddOnsPriceMonthly.forEach((eachElement) => {
            eachElement.classList.toggle("hide-display")
        })
    }
}

const step4 = () => {

    // Switching the glow of the button

    let nameOfPlanSelected = ""

    if (selectedValue == 9)
        nameOfPlanSelected = 'Arcade'
    else if (selectedValue == 12)
        nameOfPlanSelected = 'Advanced'
    else if (selectedValue == 15)
        nameOfPlanSelected = 'Pro'
    else
        nameOfPlanSelected = 'Nothing Selected'

    const modeName = document.querySelector(".mode-name")
    const modePrice = document.querySelector(".mode-price")
    if (!isMonthly) {

        selectedValue = selectedValue * 10
        modeName.innerText = nameOfPlanSelected + '(Yearly)'
        modePrice.innerText = '$' + selectedValue + '/yr'
    }
    else {
        modeName.innerText = nameOfPlanSelected
        modePrice.innerText = '$' + selectedValue + '/mo'
    }


    const extraOnlineService = document.querySelector(".online-service")
    const extraLargerStorage = document.querySelector(".larger-storage")
    const extraCustomizableProfile = document.querySelector(".customizable-profile")

    const onlineServicePrice = document.querySelector(".online-service-price")
    const largerStoragePrice = document.querySelector(".larger-storage-price")
    const customizableProfilePrice = document.querySelector(".customizable-profile-price")
    let totalPrice = parseInt(selectedValue)

    extraOnlineService.classList.toggle('hide-display')
    extraLargerStorage.classList.toggle('hide-display')
    extraCustomizableProfile.classList.toggle('hide-display')

    if (onlineService) {
        extraOnlineService.classList.toggle("hide-display")

        if (isMonthlyInPickAddOns) {
            totalPrice += 1
            onlineServicePrice.innerText = '+$1/mo'
        }
        else {
            onlineServicePrice.innerText = '+$10/yr'
            totalPrice += 10
        }
    }

    if (largerStorage) {
        extraLargerStorage.classList.toggle("hide-display")

        if (isMonthlyInPickAddOns) {
            largerStoragePrice.innerText = '+$2/mo'
            totalPrice += 2
        }
        else {
            largerStoragePrice.innerText = '+$20/yr'
            totalPrice += 20
        }
    }

    if (customizableProfile) {
        extraCustomizableProfile.classList.toggle("hide-display")

        if (isMonthlyInPickAddOns) {
            customizableProfilePrice.innerText = '+$2/mo'
            totalPrice += 2
        }
        else {
            customizableProfilePrice.innerText = '+$20/yr'
            totalPrice += 20
        }
    }

    const totalBillPrice = document.querySelector(".total-bill-price")
    totalBillPrice.innerText = '$' + totalPrice

    button3.classList.toggle("button-glow")
    button4.classList.toggle("button-glow")
    pickAddOns.classList.toggle("hide-display")
    finishingUpForm.classList.toggle("hide-display")

    // Changing the next step button to confirm button
    nextStepButton.innerText = 'Confirm'
    nextStepButton.style.backgroundColor = 'hsl(243deg 100% 62%)'

    nextStepButton.removeEventListener('click', step4)
    nextStepButton.addEventListener('click', step5)

    goBackButton.removeEventListener('click', goBackTostep2)
    goBackButton.addEventListener('click', goBackTostep3)
}

const goBackTostep3 = () => {

    button3.classList.toggle("button-glow")
    button4.classList.toggle("button-glow")
    pickAddOns.classList.toggle("hide-display")
    finishingUpForm.classList.toggle("hide-display")

    nextStepButton.innerText = 'Next Step'
    nextStepButton.style.backgroundColor = 'hsl(213deg 96% 18%)'

    goBackButton.removeEventListener('click', goBackTostep3)
    goBackButton.addEventListener('click', goBackTostep2)

    nextStepButton.removeEventListener('click', step5)
    nextStepButton.addEventListener('click', step4)
}

// Step 5
const step5 = () => {

    // Hiding the display of the Finishing Up Container
    finishingUpForm.classList.toggle("hide-display")
    thankYouPage.classList.toggle("hide-display")
    nextStepContainer.classList.toggle("hide-display")
}

// When Page Reloads call Step 1
step1();